package com.nbs.app.hitungluas;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {
    TextView nameFood;
    TextView tasteFood;
    Button btnToast;
    Button btnSnackbar;
    Button btnAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Detail Food");

        Intent intent = getIntent();
        final String nameFoodText = intent.getStringExtra("name");
        String tasteFoodText = intent.getStringExtra("taste");

        nameFood = (TextView)findViewById(R.id.name_food);
        tasteFood = (TextView)findViewById(R.id.taste_food);

        nameFood.setText(nameFoodText);
        tasteFood.setText(tasteFoodText);

        btnToast= (Button)findViewById(R.id.btn_toast);
        btnSnackbar=(Button)findViewById(R.id.btn_snackbar);
        btnAlert = (Button)findViewById(R.id.btn_alert);

        final LinearLayout layoutMain = (LinearLayout)findViewById(R.id.layout_main);

        btnToast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Main2Activity.this,"Ini halaman detail "+nameFoodText,Toast.LENGTH_SHORT).show();
            }
        });
        btnSnackbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(layoutMain,"Ini halaman detail "+nameFoodText,Snackbar.LENGTH_SHORT).show();
            }
        });
        btnAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(Main2Activity.this);
                alertDialog.setTitle("Alert Dialog");
                alertDialog.setMessage("Ini halaman detail "+nameFoodText);

                alertDialog.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

                AlertDialog alert = alertDialog.create();
                alert.show();


            }
        });


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

}
