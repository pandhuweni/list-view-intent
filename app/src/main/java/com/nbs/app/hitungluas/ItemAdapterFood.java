package com.nbs.app.hitungluas;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by pandhu on 24/06/16.
 */
public class ItemAdapterFood extends BaseAdapter {
    private List<Item> itemList;
    private LayoutInflater inflater;

    public ItemAdapterFood(List<Item> itemList, Context context) {
        this.itemList = itemList;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;

        if(convertView == null){
            convertView = inflater.inflate(R.layout.item_list,parent,false);
            holder = new Holder();
            holder.textViewName = (TextView)convertView.findViewById(R.id.txt_name);
            holder.textViewTaste= (TextView)convertView.findViewById(R.id.txt_taste);
            holder.imgView = (ImageView)convertView.findViewById(R.id.img_view);
            convertView.setTag(holder);
        }else{
            holder=(Holder)convertView.getTag();
        }

        Item item = itemList.get(position);

        holder.textViewName.setText(item.getName());
        holder.textViewTaste.setText(item.getTaste());
        holder.imgView.setImageResource(item.getImage());

        return convertView;
    }
    static class Holder{
        TextView textViewName;
        TextView textViewTaste;
        ImageView imgView;
    }
}
