package com.nbs.app.hitungluas;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    String[] data = {"List A","List B", "List C", "List D"};
    private ListView listView;
    private ItemAdapterFood itemAdapterFood;
    private ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle("Custom List View Food");

        listView = (ListView)findViewById(R.id.list_view);

        itemAdapterFood = new ItemAdapterFood(getItem(),getApplicationContext());
        listView.setAdapter(itemAdapterFood);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                intent.putExtra("name",getItem().get(position).getName());
                intent.putExtra("taste",getItem().get(position).getTaste());
                startActivity(intent);
                //Toast.makeText(MainActivity.this, "Ini adalah item "+ getItem().get(position).getName(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private Item get(String name, String taste,int image){
        return new Item(name, taste,image);
    }
    private List<Item> getItem(){
        List<Item> list = new ArrayList<Item>();
        list.add(get("Mie","Gurih",R.mipmap.ic_launcher));
        list.add(get("Sambal","Pedes",R.mipmap.launcher));
        list.add(get("Manisan","Manis",R.mipmap.ic_launcher));

        return list;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
