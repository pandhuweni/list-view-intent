package com.nbs.app.hitungluas;

/**
 * Created by pandhu on 24/06/16.
 */
public class Item {
    private String name;
    private String taste;
    private int image;
    public Item(String name, String taste,int image){
        this.name=name;
        this.taste=taste;
        this.image = image;
    }
    public String getName(){
        return name;
    }
    public int getImage() {
        return image;
    }
    public String getTaste(){
        return taste;
    }
    @Override
    public String toString(){
        return "Food{" +
                "name='" + name + '\'' +
                ", taste='" + taste + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
